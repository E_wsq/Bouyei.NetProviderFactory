﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bouyei.NetFactoryCore.Protocols.WebSocketProt
{
    public class WebSocketPacket
    {
        /// <summary>
        /// 如果为true则该消息为消息尾部,如果为零则还有后续数据包;
        /// </summary>
        public bool Fin { get; set; }
        /// <summary>
        /// RSV1,RSV2,RSV3,各1位，用于扩展定义的,如果没有扩展约定的情况则必须为0
        /// </summary>
        public bool Rsv1 { get; set; }

        public bool Rsv2 { get; set; }

        public bool Rsv3 { get; set; }
        /// <summary>
        /// 0x0表示附加数据帧
        ///0x1表示文本数据帧
        ///0x2表示二进制数据帧
        ///0x3-7暂时无定义，为以后的非控制帧保留
        ///0x8表示连接关闭
        ///0x9表示ping
        ///0xA表示pong
        ///0xB-F暂时无定义，为以后的控制帧保留
        /// </summary>
        public byte OpCode { get; set; }
        /// <summary>
        /// true使用掩码解析消息
        /// </summary>
        public bool Mask { get; set; }

        public UInt32 PayloadLength { get; set; }

        public UInt32 Continued { get; set; }

        public byte[] MaskKey { get; set; }

        public UInt16 MaskKeyContinued { get; set; }

        public SegmentOffset Payload { get; set; }
    }
}
